// join-us-section.js
const JoinUsSection = (function () {

  function createStandardSection() {
    const container = document.createElement("div");

    container.className = "container";
    const headingOne = document.createElement("h1")
    headingOne.className = "headisng-one";
    headingOne.textContent = "Join Our Program";
    const headingTwo = document.createElement("h3");
    headingTwo.className = "heading-two";

    headingTwo.innerHTML = `Sed do eiusmod tempor incididunt<br>ut labore et dolore magna aliqua.`;
    const emailContainer = document.createElement("div");

    emailContainer.className = "email-container";
    const emailInput = document.createElement("input");
    emailInput.type = "email";
    emailInput.className = "email-field";
    emailInput.placeholder = "Email";
    const submitButton = document.createElement("button");
    submitButton.className = "blue-button";
    submitButton.textContent = "SUBMIT";

    emailContainer.appendChild(emailInput);
    emailContainer.appendChild(submitButton);

    container.appendChild(headingOne);
    container.appendChild(headingTwo);
    container.appendChild(emailContainer);



    submitButton.addEventListener("submit", function(event) {
      event.preventDefault();
      const enteredEmail = emailInput.value;
      console.log("Entered Email:", enteredEmail);
    });
    container.remove = function () {
      container.parentNode.removeChild(container);

    };

    return container;

  }
  function create(type) {

    if (type === 'standard') {

      return createStandardSection();

    } else if (type === 'advanced') {

      const section = createStandardSection();

      section.querySelector('.heading-one').textContent = 'Join Our Advanced Program';
      section.querySelector('.blue-button').textContent = 'Subscribe to Advanced Program';
      return section;

    }
    return null;
  }
  return {
    create,
  };

})();
export default JoinUsSection;