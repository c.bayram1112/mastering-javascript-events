// main.js

import JoinUsSection from './join-us-section.js';

const SectionCreator = (function () {

  function create(type) {
    if (type === 'standard') {
      return JoinUsSection.createStandardSection();

    } else if (type === 'advanced') {
      const section = JoinUsSection.createStandardSection();

      section.querySelector('.heading-one').textContent = 'Join Our Advanced Program';
      section.querySelector('.blue-button').textContent = 'Subscribe to Advanced Program';

      return section;
    }
    return null;

  }

  return {
    create,

  };

})();


document.addEventListener("DOMContentLoaded", function() {
  const standardSection = JoinUsSection.create('standard');
  const appSectionImageCulture = document.querySelector(".app-section.app-section--image-culture");
  appSectionImageCulture.insertAdjacentElement('afterend', standardSection);

});